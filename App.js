import React from "react";
import {StyleSheet, Text, View} from 'react-native';
import { SafeAreaView } from "react-native";
import ModuleRoutes from "./src/main_routes";

const App = () => {
	return(
		<>
        	<ModuleRoutes />
		</>
      
  )
};

export default App;