import React from "react";
import { View, StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';



export default function CustomText(props) {
  return (
    <View style={styles.container}>
        <Text style={props && props.style ? props.style: ''}>{props && props.text ? props.text:''}</Text>
    </View>
  );
}


const styles = StyleSheet.create({
  container:{
    flex:1,
  }
});


