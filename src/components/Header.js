import React from "react";
import { TouchableOpacity, View, Image, StyleSheet,Text } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { Header } from "react-native-elements";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

import blueVersion from "../assets/stylesheet/colors";
//import { image } from "../assets/images/header_images";


export default function CustomHeader(props) {
  //console.log('custom header:', props);

  return (
    <View style={styles.container}>
      <Header
        containerStyle={{
          backgroundColor: blueVersion.primary,
          borderBottomColor: blueVersion.primary,
          height:100,
          display:'flex',
          justifyContent:'center',
        }}
        leftComponent={
          <Ionicons name="scan-outline" size={40} color={blueVersion.secondary} />
        }
        centerComponent={{
          text: props.title,
          style: {
            color: blueVersion.secondary,
            backgroundColor: blueVersion.primary,
            textTransform: "uppercase",
            fontSize: hp('1.5%'),
            marginTop:10
          },
        }}
        rightComponent={
          <Image style={{ height: 40, width: 40,borderRadius:10 }} source={require("../assets/images/header_images/image.png")} />
        }
      />
    </View>
  );
}


const styles = StyleSheet.create({
  container:{
   alignItems:'center'
  }
});
