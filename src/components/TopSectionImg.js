import React from "react";
import { View, Image, StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";




export default function TopSectionImg(props) {
  //console.log('custom header:', props);

  return (
    <View style={styles.container}>
          <Image style={styles.img} 
          source={require("../assets/images/images/IllustrationFastCash.png")} 
          resizeMode="contain"
          />
    </View>
  );
}


const styles = StyleSheet.create({
  container:{
    flex:1,
    marginHorizontal:20
  },
  img:{
    width: wp('30%'),
    height:hp('10%')
  }
});
