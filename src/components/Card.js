import React from "react";
import { View, Text, StyleSheet } from 'react-native';
import { Card, Button, Icon } from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import tailwind from 'tailwind-rn';
import tw from 'tailwind-react-native-classnames';

import blueVersion from "../assets/stylesheet/colors";
import CustomButton from "./Button";


const users = [
    {
       name: 'brynn',
       avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
    }
]

export default function CustomCard(props) {
  //console.log('custom header:', props);

  return (
    <View style={tailwind('flex-1')}>
         <Card containerStyle={props && props.cardOutline ? {...styles.cardOutline,paddingLeft:5}: {...styles.card,marginHorizontal:20}}>
           <View style={tw.style('flex-row','justify-between','items-center')}>
                <View>
                  <Card.Title style={props && props.cardOutline ? styles.titleWhite:styles.titlteInverse}>
                    {props.title}
                  </Card.Title>
                </View>
               {false && ( 
                    <View style={tw.style('justify-center','items-center','py-1','px-2','rounded-2xl',{backgroundColor:blueVersion.badge})}>
                        <Text style={props && props.cardOutline ? styles.txtWhite:styles.txtInverse}>Step 3/4</Text>
                    </View>
                )}
                </View>
           <View>
              
                <Text style={props && props.cardOutline ? styles.txtWhite:styles.txtInverse}>
                    {props.description}
                </Text>
               
               <View style={{height: 60}}>
                   {props.bottomComponent}
               </View>
           </View>
        </Card>
    </View>
  );
}


const styles = StyleSheet.create({
  card:{
    flex:1,
    borderRadius:12,
    backgroundColor:blueVersion.secondary,
    borderColor:blueVersion.cardborderColor
  },
  cardOutline:{
    flex:2,
    borderRadius:12,
    backgroundColor:blueVersion.primary,
    borderColor: blueVersion.primary,
    elevation:0,
  },
  txtWhite:{
    color:blueVersion.secondary,
    marginBottom:2,
    fontSize:hp('1.5%'),
    fontWeight:'400'
  },
  txtInverse:{
    color:blueVersion.black,
    marginBottom:2,
    fontSize:hp('1.5%'),
    fontWeight:'400'
  },

  titleWhite:{
    color:blueVersion.secondary,
    textAlign:'left',
    textTransform:'capitalize',
    fontSize:hp('2%'),
    marginBottom:5,
    fontWeight:'700'
  },
  titlteInverse:{
    color:blueVersion.black,
    textAlign:'left',
    textTransform:'capitalize',
    fontSize:hp('2%'),
    marginBottom:5,
    fontWeight:'700'
  },

});


