import React from "react";
import { View, Text, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import blueVersion from "../assets/stylesheet/colors";



export default function CustomButton(props) {
  console.log('CustomButton:',props)

  return (
    <View style={styles.container}>
        <Button
            onPress={props.onPress}
            title={props && props.title ? props.title: ''}
            type={props && props.type ? props.type : 'solid'}
            containerStyle={props && props.type == 'outline' ? styles.outlineBtn : props.type =='plain' ? styles.plainBtn : styles.solidBtn}
       />
    </View>
  );
}


const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  outlineBtn:{
      backgroundColor:blueVersion.secondary,
      color:blueVersion.primary,
      borderWidth:0,
      // width:wp('40%')
      // width: 150,
  },
  solidBtn:{
      backgroundColor:blueVersion.primary,
      borderRadius: 10
      // width:wp('40%')
      // width: 150,
  },
  plainBtn:{
    backgroundColor:blueVersion.secondary,
    borderColor:blueVersion.primary,
    borderWidth:1,
    borderRadius: 10
    // width:wp('40%')
    // width: 150,
}
});


