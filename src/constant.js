export const COLORS = {
  primary: '#4da2ff',
  white: '#fff',
  red: 'red',
};

export const API_HOST = {
	    MERCHANT_GATEWAY: 'https://merchant-gateway.test.ideopay.in/',
	    USER_GATEWAY:"https://user-gateway.test.ideopay.in/",
		DIGI_API:"https://py-data.test.ideopay.in/",
		TXN_GATEWAY: 'https://txn-gateway.test.ideopay.in/',
	};

export const API_VERSION = {
		  MERCHANT_GATEWAY_VER: 'v1',
		  USER_GATEWAY_VER:'v1',
		  DIGI_API_VER:'v2',
		  TXN_GATEWAY_VER: 'v1',
	};


export const CONFIG_KEYS = {
		  CODEPUSH: 'v1',
		  BUGSNAG:'v1',
		  RECAPTCHA:'v1',
		  CLIENT_ID:"j7kFApEJFBKc5zHPOuXfKjvbzXDvk0rX",
		  CAPTCHA_SITE_KEY: "6LdZgpccAAAAALibhFUEF9A9t0auLe7dlYK9x-aN",
	};

export const IMG_PATH = 'https://samples.test.ideopay.in/merchant-application-react-native';

// https://samples.test.ideopay.in/merchant-application-react-native
// https://samples.test.kissht.com/merchant-application-react-native