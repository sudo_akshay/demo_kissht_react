import React from "react";
import { View,StyleSheet, ScrollView, Image, ImageBackground } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp, 
} from "react-native-responsive-screen";
import tailwind from 'tailwind-rn';
import tw from 'tailwind-react-native-classnames';


import Button from "../../components/Button";
import Card from "../../components/Card";
import Header from "../../components/Header";
import TopSectionImg from "../../components/TopSectionImg";
import Scroll from "../../components/Scroll/KeyboardAwareScrollView";
import Text from "../../components/Text";
import PayBill from "./PayBill";
import blueVersion from "../../assets/stylesheet/colors";

export default function HomeScreen() {
  

  return (
    <>
    <Header title="Kissht" />
    <Scroll contentContainerStyle={{flex:1,backgroundColor:blueVersion.secondary}}>
          <ScrollView>
            <View style={tw.style('flex-1')}>
                
                <View style={tw.style('flex-1')}>
                  <View style={tw.style('rounded-b-3xl','flex-col'),styles.background}>
                      
                      <View style={tw.style('mb-2')}>
                        <View style={{height: hp('10%')}}>
                          <TopSectionImg />
                        </View>

                        <View style={{height: hp('20%')}}>
                          <Card 
                          title="FastCash"
                          description="Get Instant loan upto ₹30,000 with a revolving line of credit for 2 years."
                          cardOutline={true}
                          bottomComponent={
                            <View style={tw.style('flex-1','flex-row')}>
                                <View style={tw.style('flex-1','justify-center','items-start')}>
                                      <Button
                                      title="Get Instant Loan"
                                      type="outline"
                                      onPress={()=>{
                                          console.log('pressed');
                                      }}
                                      style={tw.style('flex-1')}
                                    />
                                </View>
                              {false && (
                                    <View style={tw.style('flex-1','justify-center','items-end')}>
                                    <Image style={tw.style('flex-1')} 
                                    source={require("../../assets/images/images/Icon.png")} 
                                    resizeMode="contain"
                                    />
                                  </View>
                                )}
                          </View>
                          }
                          />
                        </View>
                    </View>

                  </View>

                  <View style={styles.cardSection}>
                        <View style={{height: hp('20%')}}>
                                <Card 
                                title="Vouchers"
                                description="Get your favourite shopping vouchers upto ₹18,000 and redeem at online stores."
                                cardOutline={false}
                                bottomComponent={
                                  <View style={tw.style('flex-1','flex-row')}>
                                      <View style={tw.style('flex-1','justify-center','items-start')}>
                                            <Button
                                            title="Get Voucher"
                                            type="plain"
                                            onPress={()=>{
                                                console.log('pressed');
                                            }}
                                            style={tw.style('flex-1')}
                                          />
                                      </View>
                                    {true && (
                                          <View style={tw.style('flex-1','justify-center','items-end')}>
                                          <Image style={tw.style('flex-1')} 
                                          source={require("../../assets/images/images/Icon.png")} 
                                          resizeMode="contain"
                                          />
                                        </View>
                                      )}
                                </View>
                                }
                                />
                        </View>

                        <View style={{height: hp('20%')}}>
                                <Card 
                                title="Buy Now Pay Later"
                                description="You can request for instant credit of ₹5,000 to spend now and pay later."
                                cardOutline={false}
                                bottomComponent={
                                  <View style={tw.style('flex-1','flex-row')}>
                                      <View style={tw.style('flex-1','justify-center','items-start')}>
                                            <Button
                                            title="Activate Now"
                                            type="plain"
                                            onPress={()=>{
                                                console.log('pressed');
                                            }}
                                            style={tw.style('flex-1')}
                                          />
                                      </View>
                                    {true && (
                                          <View style={tw.style('flex-1','justify-center','items-end')}>
                                          <Image style={tw.style('flex-1')} 
                                          source={require("../../assets/images/images/Icon2.png")} 
                                          resizeMode="contain"
                                          />
                                        </View>
                                      )}
                                </View>
                                }
                                />
                      </View>
                             
                  </View>

                  <View style={tw.style('mt-3','mx-5')}>
                   <PayBill />
                  </View>

                  <View style={styles.referFriendSection}>
                      <Image 
                      style={tw.style('flex-1','w-full')}
                      source={require('../../assets/images/backgroundImg/BG.png')} 
                      resizeMode="stretch"
                      >
                      </Image>
                      <View style={tw.style('absolute','top-7','left-5')}>
                        <Text style={{color:blueVersion.black,fontSize: hp('2.5%'),fontWeight:'700'}} 
                        text="Refer a friend"></Text>
                        <Text style={{color:blueVersion.black,fontSize: hp('1.8%'),fontWeight:'400',width:wp('60%')}} 
                        text="Do your friend needs an instant loan, let them know about Kissht."></Text>

                      <View style={tw.style('mt-2','flex-1','justify-start','items-start')}>
                        <Button
                        title="Invite Friend"
                        type="outline"
                         onPress={()=>{
                                    console.log('pressed');
                           }}
                          // style={{borderWidth:1}}
                        />
                      </View>
                      </View>
                      <View style={tw.style('absolute','top-4','right-4')}>
                          <Image 
                          style={{flex:1,width:wp('40%')}}
                          source={require('../../assets/images/backgroundImg/Art.png')} 
                          resizeMode="stretch"
                          >
                      </Image>
                      </View>
                  </View>
                </View>
            </View>
          </ScrollView>
    </Scroll>
    </>
  );
}


const styles = StyleSheet.create({
  cardSection:{
    // flex:1
  },
  payBillSection:{
    marginHorizontal:20,
    marginTop:10
    // flex:1
  },
  referFriendSection:{
    flex:1,
    position:'relative',
    height:hp('20%'),
    marginTop:10
  },
  background: {
    backgroundColor:blueVersion.primary
  }
})
