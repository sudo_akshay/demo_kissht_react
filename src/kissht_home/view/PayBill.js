import React from "react";
import { View,StyleSheet, ScrollView, Image, Pressable, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import tailwind from 'tailwind-rn';
import tw from 'tailwind-react-native-classnames';

import blueVersion from "../../assets/stylesheet/colors";
import Button from "../../components/Button";
import Text from "../../components/Text";

export default function PayBill() {

let tempData=[
    {
        id:1,
        title:'Mobile',
        iconName:'mobile-alt'
    },
    {
        id:2,
        title:'Electricity',
        iconName:'lightbulb'
    },
    {
        id:3,
        title:'DTH',
        iconName:'tv'
    },
    {
        id:4,
        title:'Gas',
        iconName:'gas-pump'
    },
    {
        id:5,
        title:'Broadband',
        iconName:'wifi'
    },
    {
        id:6,
        title:'Landline',
        iconName:'phone-alt'
    },
    {
        id:7,
        title:'Fastag',
        iconName:'car'
    },
    {
        id:8,
        title:'View All',
        iconName:'angle-right'
    }
]
  
  return (
    <View style={tailwind('flex-1')}>
        <Text style={{color:blueVersion.black,fontSize: hp('2%'),fontWeight:'700'}} text="Pay Bills"></Text>
        <View style={tw.style('flex-1','flex-row','justify-around','flex-wrap','my-2')}>
           {tempData.map((item,index)=>{

               return(
                   <View key={index} style={tw.style('w-1/4')}>
                        <TouchableOpacity style={tw.style('flex-1','items-center')}>
                            <View style={tw.style('flex-1','my-1','py-4','px-2','w-4/5',
                            'justify-center','items-center','rounded-xl',
                            {backgroundColor:blueVersion.smallCardBackColor})}>
                                <FontAwesome5 name={item.iconName} size={28} color={blueVersion.primary} />
                            </View>
                            <View style={tw.style('flex-1')}>
                                <Text style={{color:blueVersion.black,fontSize: hp('1.5%'),fontWeight:'700'}} text={item.title}></Text>
                            </View>
                        </TouchableOpacity>
                   </View>
               )
           })}
        </View>
    </View>
  );
}
