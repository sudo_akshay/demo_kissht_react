import React from "react";
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import HomeScreen from "../view/HomeScreen";
import LoanScreen from "../view/LoanScreen";
import LaterPayScreen from "../view/LaterPayScreen";

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'home' : 'home-outline';
            } else if (route.name === 'LoanScreen') {
              iconName = focused ? 'layers-sharp' : 'layers-outline';
            } else if (route.name === 'LaterPayScreen') {
              iconName = focused ? 'infinite' : 'infinite-outline';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}
      >
        <Tab.Screen name="Home" component={HomeScreen} options={{title:'Kissht'}} />
        <Tab.Screen name="LoanScreen" component={LoanScreen} options={{title:'Loans'}} />
        <Tab.Screen name="LaterPayScreen" component={LaterPayScreen} options={{title:'Pay Later'}} />
      </Tab.Navigator>
  );
};

export default BottomTabNavigator;
