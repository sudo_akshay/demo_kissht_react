//https://stackoverflow.com/questions/34315274/react-native-detect-dev-or-production-env
import {DEV_URL, PROD_URL} from "@env";

const devEnv = {
    BACKEND_URL:DEV_URL
}

const prodEnv = {
    BACKEND_URL:PROD_URL
}

export default __DEV__ ? devEnv : prodEnv;