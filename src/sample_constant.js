export const API_HOST = {
        MERCHANT_GATEWAY: 'https://merchant-gateway.ideopay.in/',
        USER_GATEWAY:"https://user-gateway.ideopay.in/",
        DIGI_API:"https://py-data.ideopay.in/",
        TXN_GATEWAY: 'https://txn-gateway.ideopay.in/',
    }

export const API_VERSION = {
        MERCHANT_GATEWAY_VER: 'v1',
        USER_GATEWAY_VER:'v1',
        DIGI_API_VER:'v2',
        TXN_GATEWAY_VER: 'v1',
    }

export const CONFIG_KEYS = {
        CODEPUSH: 'v1',
        BUGSNAG:'v1',
        RECAPTCHA:'v1',
        CLIENT_ID:"odaO8vzDy2q1LI8UZmPyJJGpjBWzKc9P",
        CAPTCHA_SITE_KEY: "6LfJgpccAAAAAMgN1Focqw-RCWpL2__H1BK4ztaP",
    }
