// import {ENVIRONMENT} from "@env";
import {API_HOST, API_VERSION} from '../src/constant';

let HOST = API_HOST;
let VERSION = API_VERSION;

export const API_URL = {
  Get_Transactions: HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/transactions',
  Get_Settlements: HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/settlements',
  Get_Settlements_Details:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/settlements',
  Get_Bank_Details: HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/bank-accounts/ad',
  Send_Otp: HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/users/send-otp',
  Auth: HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/users/authentication',
  Merchant_OnBoard: HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/users/merchants/onboard',
  Customer_Documents: HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/documents',
  Doc_Upload: HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/documents/upload-document',
  Verify_Pan: HOST.USER_GATEWAY+"api/"+VERSION.USER_GATEWAY_VER+"/users/verify-pan",
  Update_User:HOST.USER_GATEWAY+"api/"+VERSION.USER_GATEWAY_VER+"/users",
  Get_Merchant_User:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/details',
  Update_Merchant_User:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants',
  Address:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/users/address',
  Patch_Address:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants',
  Get_User_Bank_List:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/users/bank-accounts',
  Get_IFSC:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/ifsc',
  BANK:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/users/bank-accounts',
  Check_Eligibility:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/check-eligibility',
  Post_Merchant_OnLoad_Data:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/onload-data',
  Assign_QR:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/qr-code/assign',
  Refresh_User:HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/users/refresh-token',
  Refresh_Merchant:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/refresh-token',
  Register_Merchant: HOST.DIGI_API+ 'api/' + VERSION.DIGI_API_VER+'/customer/register',
  DIGI_Messages: HOST.DIGI_API+ 'api/' + VERSION.DIGI_API_VER+'/customer/messages',
  DIGI_DateTime: HOST.DIGI_API+ 'api/' + VERSION.DIGI_API_VER+'/customer/last-updated-datetime',
  TXN_Get_Offer: HOST.TXN_GATEWAY+ 'api/' + VERSION.TXN_GATEWAY_VER+'/transactions/offers',
  TXN_User_Consent: HOST.TXN_GATEWAY+ 'api/' + VERSION.TXN_GATEWAY_VER+'/transactions/user-consent',
  TXN_Check_Eligibility: HOST.TXN_GATEWAY+ 'api/' + VERSION.TXN_GATEWAY_VER+'/transactions/check-txn-eligibility',
  Get_Home_Banner:HOST.MERCHANT_GATEWAY + 'api/' + VERSION.MERCHANT_GATEWAY_VER + '/merchants/home-screen',
  Get_Payment_List: HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/users/get-payment-list',
  GET_Payable_Details:HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/users/emi-payment/user/payable-details',
  GET_Payable_Options:HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/users/get-payment-options',
  Payment_Initate:HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/payments/initiate',
  Payment_Verify:HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/payments/verify',
  DIGI_Location: HOST.DIGI_API+ 'api/' + VERSION.DIGI_API_VER+'/customer/location_coordinates',
  Loan_History: HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/loans',
  Loan_Details_Extended: HOST.USER_GATEWAY + 'api/' + VERSION.USER_GATEWAY_VER + '/loans/transactions/',
  // {transaction_reference_number}/extended
};