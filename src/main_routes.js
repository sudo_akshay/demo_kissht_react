import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { navigationRef } from './navigation';
//import MpanelRoutes from './mpanel/routes';
//import TxnRouteApp from './txn_user_flow/routes';
//import PaymentRouteApp from './payment/routes';
import BottomTabNavigator from './kissht_home/bottomroutes/bottomTabNavigators';

const Stack = createStackNavigator();

const ModuleRoutes = () => {
    return (
        <NavigationContainer ref={navigationRef}>
			 <Stack.Navigator>
			 	<Stack.Screen name="bottomTabNav" component={BottomTabNavigator} options={{ headerShown: false }} /> 				
				{/* <Stack.Screen name="mpanelScreen" component={MpanelRoutes} options={{ headerShown: false }}/>
				<Stack.Screen name="txnscreen" component={TxnRouteApp} options={{ headerShown: false }} />
				<Stack.Screen name="paymentScreen" component={PaymentRouteApp} options={{ headerShown: false }}/> */}
				
			</Stack.Navigator>
		</NavigationContainer>
    );
};

export default ModuleRoutes;
