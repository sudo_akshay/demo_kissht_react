const blueVersion = {
  primary: '#0075FF',
  secondary: '#ffffff',
  badge:'#FFDC3D',
  black:'#2E2E2E',
  cardborderColor:'#F0F0F0',
  smallCardBackColor:'#F1F8FF'
};

export default blueVersion;
