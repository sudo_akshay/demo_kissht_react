import React, { useState } from 'react';
// import axiosInstance from './axios';
import {
	StyleSheet,
	ScrollView,
	View,
	Text,
	Image,
	TextInput,
	Button,
	TouchableOpacity,
	Platform,
	ToastAndroid,
	Alert,
  } from 'react-native';
import { Merchant_OnBoard, Digi_Register, Digi_Messages, Digi_DateTime, Digi_Location } from "../mpanel/actions/mpanel_action";
import DeviceInfo from 'react-native-device-info';
import RNAdvertisingId from 'react-native-advertising-id';
import GetLocation from 'react-native-get-location'
import imeiModule from "../components/ImeiModule";
import Loader from '../components/Loader';
import { set } from 'react-hook-form';
import {ENVIRONMENT} from "@env";
import {CONFIG_KEYS} from '../constant';
import { Get_Merchant_User} from "../txn_user_flow/redux/actions/txn_action";
import AsyncStorage from "@react-native-async-storage/async-storage";
import SmsAndroid from "react-native-get-sms-android";
import { cos } from 'react-native-reanimated';
import Bugsnag from '@bugsnag/react-native';


export const AuthContext = React.createContext();
export const MyProvider = (props) => {
	const { token, lat, long } = props
	const [key, setKey] = useState(token || '')
	const [latLong, setlatLong] = useState({ lat, long })
	// axiosInstance.defaults.headers.common.Authorization = `Bearer ${props.token || ''}`;
	return (
		<AuthContext.Provider value={{token: key, latLong, updateToken: (token) => setKey(token)}}>
			{props.children}
		</AuthContext.Provider>
	)
}


function matchString(string) {        
	var result = string.match(/FAASOS|LLBILL|HPGASa|ADHAAR|^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/gmi);
	if(result == null){
			return false;
	}else{
			return true;
	}
}

function timeConverter(UNIX_timestamp){
	var a = new Date(UNIX_timestamp);
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var year = a.getFullYear();
	var month = a.getMonth()+1;
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = year + '-' + (month.toString().length > 1 ? month : '0'+month) + '-' + (date.toString().length > 1 ? date : '0'+date) + ' ' + hour + ':' + min + ':' + sec ;
	return time;
  }

const Readsms_list = async (userRefNum, globalDeviceId) => {
	var filter = {
		box: "inbox",
	};

	SmsAndroid.list(
	JSON.stringify(filter),
	(fail) => {
		//console.log("Failed with this error: " + fail);
	},
	(count, smsList) => {
		var arr = JSON.parse(smsList);
		var databox = [];

		arr.forEach(function (object) {
			res = matchString(object.address)
			if (res == false){
				databox.push(object);
			}            
		});

		var findata = [];
		var smsobject = {};
		smsobject['messages'] = [];
		
		var message_last_updated_time = false;
		Digi_DateTime(userRefNum, globalDeviceId).then((res) => {
			if (res.success == true){
				for (let i = 0; i < databox.length; i++){

					if((res.data.message_last_updated_datetime == "") || (res.data.message_last_updated_datetime !== "" && timeConverter(databox[i]["date"]) > res.data.message_last_updated_datetime) ) {
						if (databox[i]["address"].toString().length > 0){
							var sender_id = "";
							sender_id = (databox[i]["address"].split('-'))[1];
							if(sender_id === undefined && databox[i]["address"].length > 6){
								sender_id = databox[i]["address"].substring(2);	
							}
							if (sender_id && sender_id.toString().length <= 6){
								var msgobject = {};
								msgobject["full_sender_id"] = databox[i]["address"];
								msgobject["message"] = databox[i]["body"].replace(/\s+/gms, " ");
								msgobject["message_timestamp"] = timeConverter(databox[i]["date"]);
								msgobject["mobile_number"] = databox[i]["service_center"];
								msgobject["sender_id"] = sender_id;
								// text += databox[i];
								smsobject['messages'].push(msgobject);
							}
						} 
					}	
				}
				smsobject["user_reference_number"] = userRefNum;
				smsobject["global_device_id"] = globalDeviceId;

				//console.log("msg=================FINALSMS===>",smsobject['messages'].length);
				// console.log("msg=================FINALSMS===>",smsobject);

				if(smsobject['messages'].length > 0){
					Digi_Messages(smsobject).then((res) => {
						if (res.success == true){
							//console.log("msg=====success===============>",res);
						}
						else {
							//console.log('SMS Upload failed!');
						}
					})
				}
			}
			else {
				//console.log('SMS DATE TIME Failed!');
			}
		})
	});
};

export const digi_location = async (Data) => {
	// console.log(Data);
	Digi_Location(Data).then((res) => {
		// console.log(res);
		if (res.success == true){
			console.log("msg=====success Location API===============>",res);

		}
		else {
			console.log('Digi Location Failed!');
			Bugsnag.notify(res);
		}
	})
}

export const digi_register_call = async (merchantOnboardUserData, register_data) => {
	Get_Merchant_User().then(async res => {
		if(res.success == true){
			//console.log("GET MERCHANT USER=====>>>", res.data.merchant);
			await AsyncStorage.setItem("Merchant_Details", JSON.stringify(res.data));
			//console.log("CATCH",res.data.user);
			var userdata = {}
			userdata["first_name"] = res.data.user.first_name;
			userdata["last_name"] = res.data.user.last_name;
			userdata["mobile_number"] = String(res.data.user.mobile_number);
			userdata["email"] = res.data.user.email;
			userdata["user_id"] = res.data.user.user_id;
			userdata["user_reference_number"] = res.data.user.user_reference_number;
			
			//console.log("userdata==============",userdata);
			//console.log("register_data=========",register_data);
		
			Digi_Register(register_data, userdata).then(async (resp) => {
				//console.log("DIGI",resp);
				if (resp && resp.data && resp.data.global_device_id){
					await AsyncStorage.setItem("Global_Device_Id", resp.data.global_device_id);
					//console.log("Global_Device_Id", resp.data.global_device_id);
					
					var sms = await Readsms_list(userdata["user_reference_number"], resp.data.global_device_id);
					var locdata = {};
					locdata['user_reference_number'] = userdata["user_reference_number"];
					locdata['global_device_id'] = resp.data.global_device_id;
					locdata['latitude'] = merchantOnboardUserData.latitude;
					locdata['longitude'] = merchantOnboardUserData.longitude;
					locdata['coordinates_source'] = 'gps';
					digi_location(locdata);
					//navigate("UserLandingScreen");
				}
				else {
					//console.log(resp)
					ToastAndroid.show('Global Device Id not found!', ToastAndroid.SHORT);
					//navigate("UserLandingScreen");
				}
			})
		}else{
			ToastAndroid.show('Some Error Took Place!', ToastAndroid.SHORT);
			//console.log('Some Error Took Place!');
			navigate("UserLandingScreen");
		}
	});
}

export const getUserDeviceData = async () => {
	var data = {};
	
	// IEMI Number 
	const OsVer = Platform.constants['Release'];
	const Osversion = parseFloat(OsVer);
	if (Osversion < 10){
		try {
			// const deviceImei = await imeiModule.getImei();
			// console.log(deviceImei);
			// data['Device_Imei'] = deviceImei;
			const IMEI = require('react-native-imei');
			await IMEI.getImei().then(imeiList => {
			//console.log("imeiList", imeiList);
				data['Device_Imei'] = imeiList[0];
			});
		} catch (error) {
			//console.log(error);
		}				
	}
	else {
		data['Device_Imei'] = "";
		//console.log("Android Version -", OsVer);
	}

	// Device info for Android ID 
	await DeviceInfo.getAndroidId().then((androidId) => {
		try {
			data['Android_Id'] = androidId;	
		} catch (error) {
			console.log(error);
		}
	});
		
	try {
		var manufacturer = await DeviceInfo.getManufacturer()
		// .then((manufacturer) => {
		// 	//setmanufacturer(manufacturer);
		// 	//console.log(manufacturer);
		// });  
	} catch (error) {
		//console.log(error);
	}
		
	try {
		var model = DeviceInfo.getModel();
		//setmodel(model);
		//console.log(model);
	} catch (error) {
		//console.log(error);
	}

	// Advertising ID  
	await RNAdvertisingId.getAdvertisingId().then(response => {
		data['Advertising_Id'] = response.advertisingId;
	}).catch(error => console.log(error));

	// Latitude and Longitude
	await GetLocation.getCurrentPosition({
		enableHighAccuracy: false,
		timeout: 5000,
	}).then(location => {
		data['Latitude'] = location.latitude;
		data['Longitude'] = location.longitude;
	}).catch(error => {
		const { code, message } = error;
		//console.log(code, message);
	})
	
	data["Client_id"] = CONFIG_KEYS['CLIENT_ID'];

	// AsyncStorage
	try {
		ScreenValue = JSON.stringify(data);
		await AsyncStorage.setItem("User_data", ScreenValue);	
	} catch (error) {
		//console.log(error);
	}
	
	//console.log(data);
	var gdi = await AsyncStorage.getItem('Global_Device_Id');
	const merchantOnboardUserData = {
		imei_number: data["Device_Imei"],
		advertising_id: data["Advertising_Id"],
		android_id: data["Android_Id"],
		global_device_id: gdi ? gdi :"",
		latitude: data["Latitude"],
		longitude: data["Longitude"],
		client_id: data["Client_id"]
	}
	var userToken = await AsyncStorage.getItem('User_Token');
	
	var register_data = {};
	register_data["device_model"]= model;
	register_data["device_brand"]= manufacturer;
	register_data["os_info"]=OsVer;
	register_data["android_id"]=merchantOnboardUserData.android_id;
	register_data["imei"]= merchantOnboardUserData.imei_number;
	register_data["advertising_id"]=merchantOnboardUserData.advertising_id;
	//console.log("MERCHANTONBOARD===>>>>>>>>>>>>>", merchantOnboardUserData);
	return {userToken, merchantOnboardUserData, register_data};
	//digi_register_call(register_data);
}