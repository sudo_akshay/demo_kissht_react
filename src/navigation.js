import React from "react";
export const navigationRef = React.createRef();
let navigationObj = {
  //componentname:screenname,
  'TxnRouteApp': "txnscreen",
  'MpanelRoutes': "mpanelScreen",
  'SplashScreen': "splash_screen",
  'UserLogin': "login_screen",
  'TransactionScreen': "transaction_screen",
  'TrnxAndSettleScreen': "tnx_settle_screen",
  'OtpScreen': "otp_screen",
  'SettlementScreen': "settlement_screen",
  'MyAccountScreen': "myaccount_screen",
  'DrawerNavigationScreen': "drawer_nav_screen",
  'DummyLayout': "dummy_screen",
  'TabView': "tab_screen",
  'ViewQrcodeScreen': "viewqrcode_screen",
  'ContactUsScreen': "contactus_screen", 
  "AddPanScreen": 'add_pan',
  "KycVerifyScreen" :"verify_kyc",
  "CameraKyc": "camera_kyc",
  "AddBankScreen":"add_bank",
  "PermissionScreen":"permission_screen",
  "eNachScreen" : "eNach_screen",
  "CongratsScreen" : "congratulations_screen",
  "UserLandingScreen":"user_landing_screen",
  "MerchantOnBoardScreen":"merchant_onboard_screen",
  "LogoutScreen" : "logout_screen",
  "UserHomeScreen": "Userhome",
  "QrCodeScreen": "qrcode_screen",
  "ScanQrcodeScreen": "scanqrcode_screen",
  "TermsAndConditions": "termsandconditions_screen",
  "PrivacyPolicy": "privacypolicy_screen",
  "AboutUs": "aboutus_screen",
  "Faq": "faq_screen",
  "ExpenseTracker": "expense_tracker_screen",
  "LoanDetails":"viewloanDetail_screen",
  "OfferDetails":"offer_screen",
  "PaymentHistory":"paymentHistory_screen",
  "DisbursementCongratsScreen": "congratulationsDisbursement_screen",
  "PaymentRouteApp": "paymentScreen",
  "NavigateMeditator":"navigate_mediator_screen",
  "LoanDetailScreen": "loanDetail_screen",
  "LoanHistoryScreen": "loanHistory_screen",
  "UserHome": "user_home_screen",
  "TrainingVideoScreen":"trainingVideo_screen",
};

export function navigate(screenName: string, params?: object) {
  try {
    if (navigationObj.hasOwnProperty(screenName)) {
      navigationRef.current?.navigate(navigationObj[screenName], params);
    } else {
      throw "Screen not listed";
    }
  } catch (err) {
    console.log("Error:", err);
  }
}
